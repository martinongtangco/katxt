package com.martinongtangco.katxt;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by Martin on 7/23/2014.
 */
public class PromoCard extends Card {
    private Activity _currentActivity;
    private Promo _promo;
    private FragmentManager _fragment;

    private TextView txtDescription;
    private Button btnRegister;

    public PromoCard(Promo promo, Context context, FragmentManager fragment, Activity currentActivity) {
        super(context, R.layout.promo_card_custom_layout);
        _fragment = fragment;
        _currentActivity = currentActivity;
        _promo = promo;
        Init();
    }

    public Promo get_promo() {
        return _promo;
    }

    private void Init() {

    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        //super.setupInnerViewElements(parent, view);
        txtDescription = (TextView) parent.findViewById(R.id.promoCardCustomDescription);
        btnRegister = (Button) parent.findViewById(R.id.promoCardCustomButtonRegister);

        txtDescription.setText(_promo.get_description());
        txtDescription.setTypeface(null, Typeface.ITALIC);

        btnRegister.setText(R.string.registerString);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterDialog registerDialog = new RegisterDialog();
                registerDialog.setPromo(_promo);
                registerDialog.onAttach(_currentActivity);
                registerDialog.show(_fragment, String.valueOf(btnRegister.getText()));
            }
        });
    }
}
