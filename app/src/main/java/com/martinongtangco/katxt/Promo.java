package com.martinongtangco.katxt;

import java.util.Date;
/**
 * Created by Martin on 7/16/2014.
 */
public class Promo {
    private int _id;
    private String _name;
    private String _description;
    private String _textCodeFormat;
    private int _amount;
    private int _duration;
    private String _durationType;
    private int _targetNumber;
    private int _categoryId;
    private String _carrier;


    public Promo(int id, String name, String description, String textCodeFormat, int amount
                    , int duration, String durationType, int targetNumber, int categoryId
                    , String carrier) {
        _id = id;
        _name = name;
        _description = description;
        _textCodeFormat = textCodeFormat;
        _amount = amount;
        _duration = duration;
        _durationType = durationType;
        _targetNumber = targetNumber;
        _categoryId = categoryId;
        _carrier = carrier;
    }

    public int get_id() {
        return _id;
    }

    public String get_name() {
        return _name;
    }

    public String get_description() {
        return _description;
    }

    public String get_textCodeFormat() {
        return _textCodeFormat;
    }

    public String getTextCode() {
        return String.format(_textCodeFormat, _amount, _duration);
    }

    public int get_amount() {
        return _amount;
    }

    public int get_duration() {
        return _duration;
    }

    public String get_durationType() {
        return _durationType;
    }

    public String getDurationFormat() {
        return _duration + " " + _durationType;
    }

    public int get_targetNumber() {
        return _targetNumber;
    }

    public int get_categoryId() {
        return _categoryId;
    }

    public String get_carrier() { return _carrier; }
}
