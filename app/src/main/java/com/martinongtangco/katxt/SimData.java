package com.martinongtangco.katxt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin on 8/15/2014.
 */
public class SimData extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String SIMCARD_TABLE = "SimCards";

    private static final String COL_ID = "Id";
    private static final String COL_CARRIER = "Carrier";
    private static final String COL_CONTACTNUMBER = "ContactNumber";
    private static final String COL_IMEI = "Imei";
    private static final String COL_ISACTIVE = "IsActive";

    public SimData(Context context) {
        super(context, SIMCARD_TABLE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_sim_table = "CREATE TABLE " + SIMCARD_TABLE + "( "
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_CARRIER + " TEXT, "
                + COL_CONTACTNUMBER + " TEXT, "
                + COL_IMEI + " TEXT,"
                +  COL_ISACTIVE + " INTEGER)";
        db.execSQL(create_sim_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    // extracts all sim cards
    public List<SimCard> getAllSimCards() {
        SQLiteDatabase db = this.getReadableDatabase();

        List<SimCard> sims = new ArrayList<SimCard>();
        String query = "SELECT " + COL_ID + ", " + COL_CARRIER + ", " + COL_CONTACTNUMBER + ", " + COL_IMEI + ", " + COL_ISACTIVE + " FROM " + SIMCARD_TABLE;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                int id = Integer.parseInt(cursor.getString(0));
                String carrier = cursor.getString(1);
                String contactNumber = cursor.getString(2);
                String imei = cursor.getString(3);
                Boolean isActive = Boolean.parseBoolean(cursor.getString(4));

                sims.add(new SimCard(id, carrier, imei, contactNumber, isActive));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return sims;
    }

    public SimCard getSimCardByPhoneNumber(String phoneNumber) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SIMCARD_TABLE,
                                new String[] { COL_ID, COL_CARRIER, COL_IMEI, COL_ISACTIVE },
                                COL_CONTACTNUMBER + "=?",
                                new String[] { phoneNumber },
                                null, null, null, null);
        if (cursor != null)
            cursor.moveToNext();

        SimCard sim = new SimCard(
                Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2),
                phoneNumber,
                Boolean.parseBoolean(cursor.getString(3)));

        cursor.close();
        db.close();

        return sim;
    }

    public SimCard insertSimCard(SimCard simCard) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_CARRIER, simCard.get_carrier());
        values.put(COL_CONTACTNUMBER, simCard.get_phoneNumber());
        values.put(COL_IMEI, simCard.get_imei());
        values.put(COL_ISACTIVE, simCard.get_isActive() ? 1 : 0);

        long newId = db.insert(SIMCARD_TABLE, null, values);
        db.close();

        simCard.set_id((int)newId);
        return simCard;
    }

    public void setSimToActive(SimCard simCard) {
        flagAllSimsToInActive();

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_ISACTIVE, 1);

        db.update(SIMCARD_TABLE, values, COL_ID + "=?", new String[] { String.valueOf(simCard.get_id())});
        db.close();
    }

    public void flagAllSimsToInActive() {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_ISACTIVE, 0);

        db.update(SIMCARD_TABLE, values, "", new String[] {});
        db.close();
    }

    public void removeSimCard(SimCard simCard) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SIMCARD_TABLE, COL_ID + "=?", new String[] { String.valueOf(simCard.get_id()) });
        db.close();
    }
}
