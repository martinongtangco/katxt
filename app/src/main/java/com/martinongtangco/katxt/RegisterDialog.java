package com.martinongtangco.katxt;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.gmariotti.cardslib.library.prototypes.LinearListView;

/**
 * Created by Martin on 7/20/2014.
 */
public class RegisterDialog extends DialogFragment {
    private Promo _promo;
    private Communicator _communicator;
    private TextView txtRegisterMessage;
    private Spinner spinRemindMe;
    private LinearLayout registerCustomReminder;

    public void setPromo(Promo promo) {
        _promo = promo;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.promo_register_dialog, null);

        txtRegisterMessage = (TextView) view.findViewById(R.id.txtRegisterMessage);
        txtRegisterMessage.setText(_promo.get_description());

        registerCustomReminder = (LinearLayout) view.findViewById(R.id.registerCustomReminder);

        List<String> remindList = new ArrayList<String>();
        remindList.add("Don't remind me");
        remindList.add(String.format("Remind when promo expires (%d %s)", _promo.get_duration(),
                                                                          _promo.get_durationType()));
        remindList.add("Auto register for me (experimental)");
        remindList.add("Custom...");

        spinRemindMe = (Spinner) view.findViewById(R.id.spinRemindMe);
        spinRemindMe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 3) {
                    registerCustomReminder.setVisibility(View.VISIBLE);
                } else {
                    registerCustomReminder.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, remindList);
        spinRemindMe.setAdapter(spinnerAdapter);

        builder.setView(view);
        builder.setTitle("Register: " + _promo.get_name())
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    // continue
                    _communicator.onDialog(_promo, 1);
                    }
                })
                .setNegativeButton(R.string.cancelString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    // cancel
                    _communicator.onDialog(_promo, 0);
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        _communicator = (Communicator) activity;
    }
}
