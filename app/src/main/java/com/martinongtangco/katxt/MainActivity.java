package com.martinongtangco.katxt;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TabHost;

import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.base.BaseCard;
import it.gmariotti.cardslib.library.view.CardListView;
import it.gmariotti.cardslib.library.view.listener.UndoBarController;

public class MainActivity extends Activity implements Communicator {
    private static final String NEW_LINE = System.getProperty("line.separator");
    public static List<Promo> _allPromo;
    public static HashMap<PromoType, List<Promo>> _categorizedPromo;
    public static List<String> currentList;
    private Spinner _promoSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // generates main tabs
        createTabView();

        buildPromoData();

        // generates mock cards
        ArrayList<Card> cards = generateMockCards();
        renderCardArrayAdapter(cards, R.id.myList);

        // Applies telephony
        ArrayList<Card> simCards = detectSimCard();
        renderCardArrayAdapter(simCards, R.id.simcardList);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // resume selected
        int i = _promoSpinner.getSelectedItemPosition();
        selectPlan(i);
    }

    private ArrayList<Card> generatePromoCards(int categoryId) {
        ArrayList<Card> cards = new ArrayList<Card>();
        List<Promo> promos = getPromoByCategory(categoryId);
        Card promoCard;
        CardHeader header;
        Context currentContext = this;
        FragmentManager fragment = getFragmentManager();
        for (Promo promo : promos) {
            promoCard = new PromoCard(promo, currentContext, fragment, this);
            promoCard.setId(String.valueOf(promo.get_id()));
            promoCard.setSwipeable(false);
            header = new CardHeader(currentContext);
            header.setTitle(promo.get_name());
            promoCard.addCardHeader(header);
            cards.add(promoCard);
        }

        return cards;
    }

    private ArrayList<Card> generateMockCards() {
        ArrayList<Card> cards = new ArrayList<Card>();
        Context currentContext = this;
        Card card;
        CardHeader header;
        for (int i = 0; i < 15; i++) {
            card = new Card(currentContext);
            card.setSwipeable(true);
            card.setId(String.valueOf(i));
            card.setTitle("aa sdfwrgadsfga sdfgasdf aertr adsfasdf adgads fg");
            card.setOnSwipeListener(getOnSwipeListener());
            card.setOnUndoSwipeListListener(getOnUndoSwipeListListener());
            header = new CardHeader(currentContext);
            header.setTitle("Some Title " + String.valueOf(i + 1));
            header.setButtonOverflowVisible(true);
            header.setPopupMenuListener(new CardHeader.OnClickCardHeaderPopupMenuListener() {
                @Override
                public void onMenuItemClick(BaseCard card, MenuItem item) {
                    Toast.makeText(getApplicationContext(),
                            "Click on " + item.getTitle() + "-" + ((Card) card).getCardHeader().getTitle(),
                            Toast.LENGTH_SHORT).show();
                }
            });

            //Add a PopupMenuPrepareListener to add dynamically a menu entry
            header.setPopupMenuPrepareListener(new CardHeader.OnPrepareCardHeaderPopupMenuListener() {
                @Override
                public boolean onPreparePopupMenu(BaseCard card, PopupMenu popupMenu) {
                    popupMenu.getMenu().add("Schedule");
                    popupMenu.getMenu().add("Remove");
                    return true;
                }
            });
            card.addCardHeader(header);
            cards.add(card);
        }

        return cards;
    }

    private UndoBarController.DefaultUndoBarUIElements getUndoBarUIElements() {
        return new UndoBarController.DefaultUndoBarUIElements() {
            @Override
            public int getUndoBarId() {
                return R.id.list_card_undobar;
            }

            @Override
            public int getUndoBarMessageId() {
                return R.id.list_card_undobar_message;
            }

            @Override
            public int getUndoBarButtonId() {
                return R.id.list_card_undobar_button;
            }
        };
    }

    private Card.OnUndoSwipeListListener getOnUndoSwipeListListener() {
        return new Card.OnUndoSwipeListListener() {
            @Override
            public void onUndoSwipe(Card card) {
            String id = card.getCardHeader().getTitle();
            Toast.makeText(getApplicationContext(),
                           id + " was restored!",
                           Toast.LENGTH_SHORT).show();
            }
        };
    }

    private Card.OnSwipeListener getOnSwipeListener() {
        return new Card.OnSwipeListener() {
            @Override
            public void onSwipe(Card card) {
            String id = card.getCardHeader().getTitle();
            Toast.makeText(getApplicationContext(),
                           id + " was removed",
                           Toast.LENGTH_SHORT).show();
            }
        };
    }

    private ArrayList<Card> detectSimCard() {
        ArrayList<Card> simCards = new ArrayList<Card>();
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String imeiSIM1 = telephonyInfo.getImeiSIM1();
        String imeiSIM2 = telephonyInfo.getImeiSIM2();
        String phoneNumberSIM1 = telephonyInfo.getPhoneNumberSIM1();
        String phoneNumberSIM2 = telephonyInfo.getPhoneNumberSIM2();

        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
        boolean isDualSIM = telephonyInfo.isDualSIM();

        TelephonyManager telephonyManager = (TelephonyManager)getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = String.valueOf(telephonyManager.getNetworkOperatorName());
        Card sim = new SimUICard(new SimCard(1, carrierName, imeiSIM1, phoneNumberSIM1, true), getBaseContext(), this);
        simCards.add(sim);

        if (isDualSIM) {
            HashMap<String, String> carrierPrefix = new HashMap<String, String>();
            carrierPrefix.put("0813", "Smart");
            carrierPrefix.put("0817", "Globe");
            carrierPrefix.put("0905", "Globe or Touch Mobile");
            carrierPrefix.put("0906", "Globe or Touch Mobile");
            carrierPrefix.put("0907", "Smart or Talk N Text");
            carrierPrefix.put("0908", "Smart or Talk N Text");
            carrierPrefix.put("0909", "Smart or Talk N Text");
            carrierPrefix.put("0910", "Smart or Talk N Text");
            carrierPrefix.put("0912", "Smart or Talk n Text");
            carrierPrefix.put("0915", "Globe or Touch Mobile");
            carrierPrefix.put("0916", "Globe or Touch Mobile");
            carrierPrefix.put("0917", "Globe");
            carrierPrefix.put("09178", "Globe");
            carrierPrefix.put("0918", "Smart");
            carrierPrefix.put("0919", "Smart or Talk n Text");
            carrierPrefix.put("0920", "Smart, Talk N Text");
            carrierPrefix.put("0921", "Smart or Talk N Text");
            carrierPrefix.put("0922", "Sun");
            carrierPrefix.put("0923", "Sun");
            carrierPrefix.put("0925", "Sun");
            carrierPrefix.put("0926", "Globe or Touch Mobile");
            carrierPrefix.put("0927", "Globe or Touch Mobile");
            carrierPrefix.put("0928", "Smart or Talk N Text");
            carrierPrefix.put("0929", "Smart or Talk N Text");
            carrierPrefix.put("0930", "Smart, Talk N Text, Red Mobile");
            carrierPrefix.put("0932", "Sun Cellular");
            carrierPrefix.put("0933", "Sun Cellular");
            carrierPrefix.put("0934", "Sun Cellular");
            carrierPrefix.put("0935", "Globe or Touch Mobile");
            carrierPrefix.put("0936", "Globe or Touch Mobile");
            carrierPrefix.put("0937", "Globe or Touch Mobile");
            carrierPrefix.put("0938", "Smart, Talk N Text, Red Mobile");
            carrierPrefix.put("0939", "Smart, Talk N Text, Red Mobile");
            carrierPrefix.put("0942", "Sun Cellular");
            carrierPrefix.put("0943", "Sun Cellular");
            carrierPrefix.put("0946", "Talk N Text");
            carrierPrefix.put("0947", "Smart");
            carrierPrefix.put("0948", "Smart or Talk N Text");
            carrierPrefix.put("0949", "Smart or Talk N Text");
            carrierPrefix.put("0973", "Extelcom");
            carrierPrefix.put("0974", "Extelcom");
            carrierPrefix.put("0977", "Next Mobile");
            carrierPrefix.put("0979", "Next Mobile");
            carrierPrefix.put("0989", "Smart or Talk N Text");
            carrierPrefix.put("0994", "Globe");
            carrierPrefix.put("0996", "Globe or Touch Mobile");
            carrierPrefix.put("0997", "Globe or Touch Mobile");
            carrierPrefix.put("0998", "Smart");
            carrierPrefix.put("0999", "Smart");
        }

        return simCards;
    }

    private void createTabView() {
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("home");
        tabSpec.setContent(R.id.homeTab);
        tabSpec.setIndicator("Home");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("offers");
        tabSpec.setContent(R.id.offersTab);
        tabSpec.setIndicator("Offers");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("sims");
        tabSpec.setContent(R.id.simTab);
        tabSpec.setIndicator("Sim Cards");
        tabHost.addTab(tabSpec);

        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)
        {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.WHITE);
        }
    }

    private void renderCardArrayAdapter(ArrayList<Card> cards, int listId) {
        CardArrayAdapter cardArrayAdapter = new CardArrayAdapter(this, cards);
        cardArrayAdapter.setUndoBarUIElements(getUndoBarUIElements());
        cardArrayAdapter.setEnableUndo(true);
        CardListView listView = (CardListView) findViewById(listId);
        if (listView != null)
            listView.setAdapter(cardArrayAdapter);
    }

    /**
     * Builds dummy data for the test.
     * In a real app this would be an adapter
     * for your data. For example a CursorAdapter
     */
    public ListAdapter buildPromoData() {
        if (_allPromo == null) {
            _allPromo = promoGenerator();
        }

        List<String> keys = new ArrayList<String>();
        List<String> values = new ArrayList<String>();
        if (_categorizedPromo == null) {
            _categorizedPromo = promoTypeGenerator();

            for(PromoType item : _categorizedPromo.keySet()) {
                if (!keys.contains(item.get_name()))
                    keys.add(item.get_name());
            }

            _promoSpinner = (Spinner) findViewById(R.id.promoSpinner);
            _promoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    selectPlan(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            Collections.sort(keys);
            ArrayAdapter<String> spinnerAdapter =
                    new ArrayAdapter<String>(this, R.layout.promo_spinner_layout, keys);
            _promoSpinner.setAdapter(spinnerAdapter);
        }

        return new ArrayAdapter<String>(
                this,
                R.layout.expandable_list_item,
                R.id.text,
                values
        );
    }

    private void selectPlan(int i) {
        String selected = _promoSpinner.getSelectedItem().toString();
//                    List<String> values = getPromoByCategoryName(selected);
//
//                    // populates list
//                    populateExpandableListView(R.id.list, values);

        ArrayList<Card> cards = generatePromoCards(i + 1); // generateMockCards();
        renderCardArrayAdapter(cards, R.id.promoList);
    }

    public static Promo getPromoByName(String promoName) {
        for(Promo promoItem : _allPromo) {
            if (promoItem.get_name() == promoName) {
                return promoItem;
            }
        }
        return null;
    }

    private HashMap<PromoType, List<Promo>> promoTypeGenerator() {
        HashMap<PromoType, List<Promo>> grouped = new HashMap<PromoType, List<Promo>>();
        grouped.put(new PromoType(1, "Call & Text"), getPromoByCategory(1));
        grouped.put(new PromoType(2, "Data"), getPromoByCategory(2));
        return grouped;
    }

    private List<Promo> getPromoByCategory(int categoryId) {
        List<Promo> promoList = new ArrayList<Promo>();
        for(Promo promo : _allPromo) {
            if (promo.get_categoryId() == categoryId)
                promoList.add(promo);
        }
        return promoList;
    }

    private List<Promo> promoGenerator() {
        List<Promo> promoList = new ArrayList<Promo>();
//        promoList.add(new Promo(1, "Big Unli Text 50", "Enjoy 7 days of unlimited tri-net texts for only P50! ", "Text %d", 50, 7, "days", 6406, 1));
//        promoList.add(new Promo(2, "All In 99", "Now you can stay connected for 7 days! Enjoy Unli All Net texts, 100 minutes of calls to Smart, Talk ‘N Text, and Sun, 80MB of surfing data and Unli Facebook for only P99!", "Allin%d", 99, 7, "days", 2827, 1));
//        promoList.add(new Promo(3, "Unli Call & Text 299", "Now, you can Call & Text like there’s no tomorrow! Enjoy unlimited calls & texts to Smart/TNT + 150 texts to all networks for only P299 valid for 15 days!", "UNLI%d", 299, 15, "days", 6406, 1));
//        promoList.add(new Promo(4, "Unli Call & Text 200", "Now, you can Call & Text like there’s no tomorrow! Enjoy unlimited calls & texts to Smart/TNT + 500 texts to all networks for only P200 valid for 7 days!", "UNLI%d", 200, 7, "days", 6406, 1));
//        promoList.add(new Promo(5, "Unli Call & Text 150", "Now, you can Call & Text like there’s no tomorrow! Enjoy unlimitied calls & texts to Smart/TNT + 100 texts to all networks for only P150 valid for 7 days!", "UNLI%d", 150, 7, "days", 6406, 1));
//        promoList.add(new Promo(6, "Unli Call & Text 100", "Now, you can Call & Text like there’s no tomorrow! Enjoy unli calls & texts to Smart/TNT + 80 texts to all networks for only P100 valid for 4 days!", "UNLI%d", 100, 4, "days", 6406, 1));
//        promoList.add(new Promo(7, "Unli Call & Text 50", "Now, you can Call & Text like there’s no tomorrow! Enjoy two days of unlimited calls to SMART/TNT/SUN, unlimited texts to all networks + FREE chat & surf (up to 30MB) for only P50!", "UNLI%d", 50, 30, "MB", 6406, 1));
//        promoList.add(new Promo(8, "Unli Call & Text 30", "Now, you can Call & Text like there’s no tomorrow! Enjoy unlimited calls to SMART/TNT/SUN, unlimited texts to all networks + FREE Chat & Surf (up to 15MB) for only P30 a day!", "UNLI%d", 30, 15, "MB", 6406, 1));
//        promoList.add(new Promo(9, "Unli Call & Text 600", "Now, you can Call & Text like there’s no tomorrow! Enjoy unli calls & texts to Smart/TNT + 2000 texts to all networks for only P600 valid for 30 days!", "UNLI%d", 600, 30, "days", 6406, 1));
//        promoList.add(new Promo(10, "Unli Call & Text 25", "Now, you can Call & Text like there’s no tomorrow! Enjoy unlimited calls to SMART/TNT, unlimited texts to SMART/TNT/SUN, 50 texts to all networks + FREE chat & surf (up to 15MB) for only P25 a day!", "UNLI%d", 25, 1, "days", 6406, 1));
//        promoList.add(new Promo(11, "Unli Call & Text 35", "Now, you can Call & Text like there’s no tomorrow! Enjoy unli calls & texts to Smart/TNT/Sun + 150 texts to all networks for only P35 a day!", "UNLI%d", 35, 1, "days", 6406, 1));
//        promoList.add(new Promo(12, "Unli Call & Text 400", "Now, you can Call & Text like there’s no tomorrow! Enjoy unli calls & texts to Smart/TNT + 1000 texts to all networks for only P400 valid for 15 days!", "UNLI%d", 400, 15, "days", 6406, 1));
//        promoList.add(new Promo(13, "Smartalk 500", "Enjoy unli calls to Smart/TNT for only P500 valid for 30 days!", "TALK%d", 500, 30, "days", 6400, 1));
//        promoList.add(new Promo(14, "Smartalk 100", "Enjoy unli calls to Smart/TNT for only P100 valid for 5 days!", "TALK%d", 100, 5, "days", 6400, 1));
//        promoList.add(new Promo(15, "All Text 20", "Enjoy unli all-net texts for just P20 a day, plus get 20 minutes of calls to SMART and TNT and free chat & surf!", "AT%d", 20, 1, "days", 2827, 1));
//        promoList.add(new Promo(16, "Lahatxt 30", "Lets you send 300 SMS to all networks and enjoy 20 mins of calls to Smart/TNT subscribers for 2 days!", "L%d", 30, 2, "days", 2266, 1));
//        promoList.add(new Promo(17, "All Text 100", "Get unlimited ALL network SMS and 100 minutes of calls to SMART and TNT.", "AT%d", 100, 7, "days", 2827, 1));
//        promoList.add(new Promo(18, "All Text 15", "Enjoy 150 texts to all networks and 5 minutes calls to Smart/TNT for just P15 valid for 1 day!", "AT%d", 15, 1, "days", 2827, 1));
//        promoList.add(new Promo(19, "Big Unli Text 200", "Enjoy 30 days of unlimited tri-net texts for only P200!", "TEXT%d", 200, 30, "days", 6406, 1));
//        promoList.add(new Promo(20, "Big Unli Text 100", "Enjoy 15 days of unlimited tri-net texts for only P100! ", "TEXT%d", 100, 15, "days", 6406, 1));
//        promoList.add(new Promo(21, "Big Calls 100", "Enjoy 200 minutes of calls to Smart and Talk ‘N Text for 7 days, for only P100! ", "CALL%d", 100, 7, "days", 6406, 1));
//        promoList.add(new Promo(22, "Big Calls 200", "Enjoy 450 minutes of calls to Smart and Talk ‘N Text for 15 days, for only P200! ", "CALL%d", 200, 15, "days", 6406, 1));
//        promoList.add(new Promo(23, "Big Calls 400", "Enjoy 950 minutes of calls to Smart and Talk ‘N Text for 30 days, for only P400! ", "CALL%d", 400, 30, "days", 6406, 1));
//        promoList.add(new Promo(24, "TRI-NET 300", "Now, a TriNet offer that is available for all three networks! Connect on Smart/Sun/TnT and enjoy unli text, 300 mins. of calls, and 30MB of mobile Internet for only P300! Valid for 30 days! ", "TRINET%d", 300, 30, "days", 2477, 1));
//        promoList.add(new Promo(25, "All Text 30", "Enjoy unli all-net texts and 30 minutes calls to Smart/TNT for just P30 valid for 2 days!", "AT%d", 30, 2, "days", 2827, 1));
//        promoList.add(new Promo(26, "TRI-NET 100", "Reconnecting with someone just became easier and better. Receive 750 to all networks, 120 mins of calls to Smart/TNT/SUN plus 35MB data for 7 days with Trinet 100. ", "TRINET%d", 100, 7, "days", 2477, 1));
//        promoList.add(new Promo(27, "Lahatxt 20", "Lets you send 250 SMS to all networks and enjoy 10 mins of calls to Smart/TNT subscribers for 1 day!", "L%d", 20, 1, "days", 2266, 1));
//        promoList.add(new Promo(28, "TRI-NET 15", "Reconnecting just became easier! Enjoy unlimited texts to all your Smart/TNT/SUN contacts with Trinet 15. ", "Trinet %d", 15, 1, "days", 2477, 1));
//        promoList.add(new Promo(29, "TRI-NET 30", "Reconnecting with someone just became easier and better. Receive 200 texts to all networks, 50 mins of calls to Smart/TNT/SUN plus 10MB data for 1 day with Trinet 30.  ", "TRINET%d", 30, 1, "days", 2477, 1));
//        promoList.add(new Promo(30, "TRI-NET 40", "Enjoy unlimited tri-net calls, texts and Facebook with Tri-net 40! ", "TRINET%d", 40, 1, "days", 2949, 1));
//        promoList.add(new Promo(31, "TRI-NET 200", "Reconnecting with someone just became easier and better. Receive 1500 texts to all networks, 250 mins of calls to Smart/TNT/SUN plus 75MB data for 15 days with Trinet 100.  ", "TRINET%d", 200, 15, "days", 2477, 1));
//        promoList.add(new Promo(32, "TRI-NET 400", "Reconnecting with someone just became easier and better. Receive 3000 texts to all networks, 500 mins of calls to Smart/TNT/SUN plus 35MB data for 30 days with Trinet 400.  ", "TRINET%d", 400, 30, "days", 2477, 1));
//        promoList.add(new Promo(33, "FB2", "Like, share, comment, and post on your phone all you want for just P2! Enjoy unlimited Facebook for 1 whole day! To avail, you must be registered to an existing prepaid promo.", "FB%d", 2, 1, "days", 6406, 2));
//        promoList.add(new Promo(34, "All In 99", "Now you can stay connected for 7 days! Enjoy Unli All Net texts, 100 minutes of calls to Smart, Talk ‘N Text, and Sun, 80MB of surfing data and Unli Facebook for only P99!", "Allin%d", 99, 7, "days", 2827, 2));
//        promoList.add(new Promo(35, "PISO APP", "Enjoy 5 hours consumable Internet on-the-go for only P1/day with the new Piso Sampling! Send PISO APP to 211 now!", "PISO APP ", 1, 1, "days", 211, 2));
//        promoList.add(new Promo(36, "LTE 50", "Unlimited Internet Browsing on 3G/LTE Network for 1 day", "LTE %d", 50, 1, "days", 2200, 2));
//        promoList.add(new Promo(37, "LTE 299", "Unlimited Internet Browsing on 3G/LTE Network for 7 days", "LTE %d", 299, 7, "days", 2200, 2));
//        promoList.add(new Promo(38, "LTE 995", "Unlimited Internet Browsing on 3G/LTE Network for30 days", "LTE %d", 995, 30, "days", 2200, 2));
//        promoList.add(new Promo(39, "LINE 10", "Line Messaging", "LINE %d", 10, 1, "days", 2200, 2));
//        promoList.add(new Promo(40, "WECHAT 10", "Chat all to Line, WeChat, WhatsApp and Facebook Messenger", "WECHAT %d", 10, 1, "days", 2200, 2));
//        promoList.add(new Promo(41, "CHATALL 299", "Chat all to Line, WeChat, WhatsApp and Facebook Messenger", "CHATALL %d", 299, 30, "days", 2200, 2));
        return promoList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialog(Promo promo, int selection) {
        switch (selection) {
            case 1: // register

                break;
            default: // close
                // do nothing for now
                break;
        }
    }
}
