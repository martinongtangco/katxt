package com.martinongtangco.katxt;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by Martin on 8/5/2014.
 */
public class SimCard {
    private int _id;
    private String _carrier;
    private String _imei;
    private String _phoneNumber;
    private Boolean _isActive;

    public SimCard(int id, String carrier, String imei, String phoneNumber, Boolean isActive) {
        _id = id;
        _carrier = carrier;
        _imei = imei;
        _phoneNumber = phoneNumber;
        _isActive = isActive;
    }

    public int get_id() {
        return _id;
    }
    public void set_id(int id) {
        _id = id;
    }

    public String get_carrier() {
        return _carrier;
    }

    public String get_imei() {
        return _imei;
    }

    public String get_phoneNumber() { return _phoneNumber; }

    public Boolean get_isActive() { return _isActive; }
}
