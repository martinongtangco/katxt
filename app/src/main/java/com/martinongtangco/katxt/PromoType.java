package com.martinongtangco.katxt;

/**
 * Created by Martin on 7/19/2014.
 */
public class PromoType {
    private int _id;
    private String _name;

    public PromoType(int id, String name) {
        _id = id;
        _name = name;
    }

    public int get_id(){ return _id; }
    public String get_name() { return _name; }
}
