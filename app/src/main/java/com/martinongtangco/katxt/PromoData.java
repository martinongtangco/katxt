package com.martinongtangco.katxt;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;

/**
 * Created by Martin on 8/15/2014.
 */
public class PromoData extends SQLiteOpenHelper {

    private static final String PROMOLIST_TABLE = "PromoList";
    private static final int DATABASE_VERSION = 1;

    private static final String COL_ID = "Id";
    private static final String COL_NAME = "Name";
    private static final String COL_DESCRIPTION = "Description";
    private static final String COL_TEXTCODEFORMAT = "TextCodeFormat";
    private static final String COL_AMOUNT = "Amount";
    private static final String COL_DURATION = "Duration";
    private static final String COL_DURATIONTYPE = "DurationType";
    private static final String COL_TARGETNUMBER = "TargetNumber";
    private static final String COL_CARRIER = "CarrierId";
    private static final String COL_PROMOTYPE = "PromoType";

    private Context _context;

    public PromoData(Context context) {
        super(context, PROMOLIST_TABLE, null, DATABASE_VERSION);
        _context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        prePopulateMSQLite(_context);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public void prePopulateData() {

    }

    private void prePopulateMSQLite(Context context) {
        DataBaseHelper dbHelper = new DataBaseHelper(context);

        try {
            dbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }

        try {
            dbHelper.openDataBase();
        }catch(SQLException sqle){
            throw sqle;
        }
    }
}
