package com.martinongtangco.katxt;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Martin on 7/20/2014.
 */
public class PromoDetailsDialog extends DialogFragment {
    private Button btnRegisterDetail, btnCloseDetail;
    private TextView txtMessage;
    private Communicator _communicator;
    private Promo _promo;

    public void setPromo(Promo promo) {
        _promo = promo;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.promo_details_dialog, null);

        txtMessage = (TextView) view.findViewById(R.id.txtMessage);
        txtMessage.setText(_promo.get_description());

        builder.setView(view);
        builder.setTitle(_promo.get_name())
        .setPositiveButton(R.string.registerString, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                _communicator.onDialog(_promo, 1);
            }
        }).setNegativeButton(R.string.closeString, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                _communicator.onDialog(_promo, 0);
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        _communicator = (Communicator) activity;
    }
}
