package com.martinongtangco.katxt;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by Martin on 8/6/2014.
 */
public class SimUICard extends Card {
    private SimCard _simCard;
    private Activity _currentActivity;

    private ImageView simCardImg;
    private TextView simCardCarrier;
    private TextView simCardDetails;
    private Button simCardActive;

    public SimUICard(SimCard simCard, Context context, Activity currentActivity) {
        super(context, R.layout.sim_card_ui);
        _simCard = simCard;
        _currentActivity = currentActivity;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        simCardCarrier = (TextView) parent.findViewById(R.id.simCardCarrier);
        simCardDetails = (TextView) parent.findViewById(R.id.simCardDetails);
        simCardActive = (Button) parent.findViewById(R.id.simCardActive);

        simCardCarrier.setText(_simCard.get_carrier());
        simCardDetails.setText("+" + _simCard.get_phoneNumber());

        setSimCardImage(parent);

    }

    private void setSimCardImage(ViewGroup parent) {
        simCardImg = (ImageView) parent.findViewById(R.id.simCardImg);
        if (_simCard.get_carrier().equalsIgnoreCase("SMART")) {
            simCardImg.setImageResource(R.drawable.smart);
        } else if (_simCard.get_carrier().equalsIgnoreCase("GLOBE")) {
            simCardImg.setImageResource(R.drawable.globe);
        } else if (_simCard.get_carrier().equalsIgnoreCase("SUN")) {
            simCardImg.setImageResource(R.drawable.sun);
        } else if (_simCard.get_carrier().equalsIgnoreCase("TOUCH MOBILE")) {
            simCardImg.setImageResource(R.drawable.tm);
        } else if (_simCard.get_carrier().equalsIgnoreCase("TALK N TEXT")) {
            simCardImg.setImageResource(R.drawable.talkntext);
        } else {
            // do nothing
            simCardCarrier.setText("\"" + _simCard.get_carrier() + "\"");
        }
    }
}
