package com.martinongtangco.katxt;

/**
 * Created by Martin on 7/20/2014.
 */
public interface Communicator {
    public void onDialog(Promo promo, int selection);
}
